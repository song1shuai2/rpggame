﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

	// Use this for initialization

	public GameObject target;
	public float attackTimer;
	public float coolDown;
	void Start () 
	{
		attackTimer = 0;
		coolDown = 2.0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (attackTimer >0) 
		{
			attackTimer -= Time.deltaTime;
		}
		if (attackTimer < 0) 
		{
			attackTimer = 0;
		}
		if (Input.GetKey(KeyCode.F)&&(attackTimer == 0)) 
		{
			Attack();
			attackTimer = coolDown;
		}
	}
	private void Attack()
	{
		float distance =  Vector3.Distance(target.transform.position,transform.position);

		Vector3 dir = (target.transform.position - transform.position).normalized;
		float direction = Vector3.Dot(dir,transform.forward);


		if (distance <= 2.5f&& direction>0f) 
		{
			EnemyHealth eh = (EnemyHealth)target.GetComponent<EnemyHealth>();
			eh.AddjustCurrrentHealth(-10);
		}


	}
}

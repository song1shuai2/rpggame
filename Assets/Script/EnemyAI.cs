﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	// Use this for initialization
	
	public int moveSpeed;
	public int rotationSpeed;

	private Transform _myTransform;
	private Transform _target;
	public float maxDistance;

	void Awake()
	{
		_myTransform = transform;
		maxDistance  = 2;

	}

	void Start () 
	{
		GameObject go = GameObject.FindGameObjectWithTag("Player");
		_target =  go.transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Debug.DrawLine(_target.position,_myTransform.position,Color.yellow);
		// Look at target;
		if (Vector3.Distance(_target.position,_myTransform.position)>maxDistance) 
		{
			_myTransform.rotation = Quaternion.Slerp(_myTransform.rotation,Quaternion.LookRotation(_target.position-_myTransform.position),rotationSpeed*Time.deltaTime);
			_myTransform.position+=_myTransform.forward* moveSpeed*Time.deltaTime;
		}

	


	}


}

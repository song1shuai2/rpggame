﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

	// Use this for initialization
	public int maxHealth = 100;
	public int curHealth = 50;

	public float healthBarLength;

	void OnGUI ()
	{
		GUI.Box(new Rect(10f,10f,healthBarLength,20f),curHealth+"/"+maxHealth);
	}

	void Start () 
	{
		healthBarLength = Screen.width/2;
	}
	
	// Update is called once per frame
	void Update () 
	{
		AddjustCurrrentHealth(0);
	}
	/*
		need to sepertate 
	 */
	public void AddjustCurrrentHealth(int adj)
	{
		curHealth += adj;


		if (curHealth <0)
		{
			curHealth = 0;

		}

		if (curHealth >maxHealth) 
		{
			curHealth = maxHealth;
		}
		if (maxHealth<1) 
		{
			maxHealth = 1;
				}

		healthBarLength = (float)(Screen.width*0.5)*(curHealth/(float)maxHealth);
		//Debug.Log(healthBarLength);
	}
}

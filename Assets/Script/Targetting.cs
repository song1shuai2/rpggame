﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Targetting : MonoBehaviour {

	// Use this for initialization

	public List<Transform> targets;
	public Transform _selectedTarget;


	void Start () 
	{
		targets = new List<Transform>();
		AddAllEnemies();
		_selectedTarget = null;

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Tab)) 
		{
			TargetEnemy();
		}
	}
	private void SortTargetByDistance()
	{
		targets.Sort(
						delegate(Transform t1,Transform t2) 
					  {
						return Vector3.Distance(t1.position,transform.position).CompareTo(Vector3.Distance(t2.position,transform.position));
						 
		});

	}
	void AddAllEnemies()
	{
		GameObject [] go =GameObject.FindGameObjectsWithTag("Enemy");
		foreach(GameObject enemy in go)
		{
			AddTarget(enemy.transform);
		}

	}
	private void TargetEnemy()
	{
		if (_selectedTarget ==null) 
		{
			SortTargetByDistance();
			_selectedTarget = targets[0];
		}
		else
		{
			int index = targets.IndexOf(_selectedTarget);
			if (index < targets.Count-1) 
			{
				index++;
			}
			// back to start
			else
			{
				index=0;
			}
			DeselectTarget();
			_selectedTarget= targets[index];
		}
		SelectTarget();

	}

	private void SelectTarget()
	{

//		PlayerAttack pa = (PlayerAttack)GetComponent<PlayerAttack>();
//
//		pa.target = _selectedTarget.gameObject;
	}
	private void DeselectTarget()
	{
		//_selectedTarget.renderer.material.color = Color.blue;
		_selectedTarget = null;
	}

	void AddTarget(Transform enemy)
	{
		targets.Add(enemy);
	}
}

﻿
public class Skill : ModifiedStat {
	private bool _know;
	public Skill()
	{
		_know = false;
		ExpToLevel = 25;
		LevelModifier = 1.1f;

	}
	public bool Known
	{
		get{return _know;}
		set{_know= value;}
	}


}
public enum SkillName
{
	Melee_Offence,
	Melee_Deffence,
	Ranged_Deffence,
	Ranged_Offence,
	Magic_Offence,
	Magic_Deffence
}

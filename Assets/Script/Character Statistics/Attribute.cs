﻿
public class Attribute : BaseStat
{

	new public const int STARTING_EXP_COST = 100;


	public Attribute()
		{
			
			ExpToLevel = 50;
			LevelModifier =1.05f;
		}


}
public enum AttributeName
{
	Might,
	Constitution,
	Nimbleness,
	Speed,
	Concentration,
	Willpower,
	Charisma

}
﻿
using System.Collections.Generic;
public class ModifiedStat : BaseStat
{
	private List<ModifyingAttribute>_mods;   //A list of Attributes that modify this stat
	private int _modValue;					// The amount added to the baseValue frome the modifiers

	public ModifiedStat()
	{
		_mods  = new List<ModifyingAttribute>();
		_modValue = 0;
			
	}
	public void AddModifier(ModifyingAttribute mod)
	{
		_mods.Add(mod);
	}
	private void CalculateModValue()
	{
		_modValue = 0;
		if (_mods.Count>0) 
		{
			foreach (ModifyingAttribute att in _mods)
			{
				_modValue+= (int)(att.attribute.AdjustedBaseValue * att.ratio);
			}
		}

	}

	public new int AdjustedBaseValue
	{
		
		get{return BaseValue + BuffValue +_modValue;}
	}
	public void Update()
	{
		CalculateModValue();
	}
	public  string GetModifingAttributeString()
	{
		string temp="";


		for (int i = 0; i <_mods.Count ; i++) 
		{
			temp+= _mods[i].attribute.Name;
			temp+= "_";
			temp+= _mods[i].ratio;
			//UnityEngine.Debug.Log(_mods[i].attribute);
			if (i<_mods.Count-1) 
			{
				temp+="|";
			}
				
		}

		return temp;
	}

}



public struct ModifyingAttribute
{
	public Attribute attribute;
	public float ratio;
    
	public ModifyingAttribute(Attribute a,float f)
	{
		attribute = a;
		ratio = f;
	}
}
